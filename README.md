# Genlib - Python library for generative models

This is a library for building and playing with generative models. All
networks are written in PyTorch.

## Installation

Create a new virtual env and install requirements:

`pip install -r requirements.txt`

Now you can install the library:

`pip install .`

And now it is ready to use.
