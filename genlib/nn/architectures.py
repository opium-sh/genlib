from abc import ABC, abstractmethod
from typing import Tuple, Callable

import torch
import torch.nn as nn
from sklearn.metrics import roc_auc_score
from torch import Tensor

from genlib.loggers import auc_logger, loss_logger
from genlib.nn.layers import TorusLayer, InverseTorusLayer
from genlib.nn.losses import (
    GANDiscriminatorCELoss,
    GANGeneratorCELoss,
    VAELoss,
    MSEReconstructionLoss1D,
    L1ReconstructionLoss1D,
)


def correlation(x, y):
    vx = x - torch.mean(x)
    vy = y - torch.mean(y)
    return torch.sum(vx * vy) / (torch.sqrt(torch.sum(vx ** 2)) * torch.sqrt(torch.sum(vy ** 2)))


class GenerativeModel(nn.Module, ABC):
    @abstractmethod
    def encode(self, x: Tensor) -> Tensor:
        pass

    @abstractmethod
    def decode(self, z: Tensor) -> Tensor:
        pass

    @abstractmethod
    def sample(self, N: int) -> Tensor:
        pass

    @abstractmethod
    def latent_sample(self, N: int) -> Tensor:
        pass

    @abstractmethod
    def train_epoch(self, dataset: Tensor):
        pass

    def train_model(self, dataset: Tensor, epochs: int) -> None:
        self.train()
        for i in range(epochs):
            self.train_epoch(dataset)
            if i % 50 == 0:
                print("\n%4d" % i, end="")
            print(".", end="")
        self.eval()

    def device(self):
        return next(iter(self.parameters())).device


class VAE(GenerativeModel):
    def __init__(
        self,
        encoder: nn.Module,
        decoder: nn.Module,
        beta: float,
        encoder_last_width: int,
        z_size: int,
        optimizer: torch.optim.Optimizer = None,
        lr: float = 0.001,
        torus_layer: bool = False,
    ):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder
        if torus_layer:
            self.z_mean_layer = TorusLayer(encoder_last_width, z_size)
            self.inverse_z_layer = InverseTorusLayer(z_size, z_size)
            self.z_std_layer = nn.Linear(encoder_last_width, z_size)
        else:
            self.z_mean_layer = nn.Linear(encoder_last_width, z_size)
            self.z_std_layer = nn.Linear(encoder_last_width, z_size)
        self.z_size = z_size
        self.beta = beta
        self.criterion = VAELoss(MSEReconstructionLoss1D(), self.beta)
        self.lr = lr
        self.torus_layer = torus_layer
        if optimizer:
            self.optimizer = optimizer
        else:
            self.optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)

    def calc_mean_logvar(self, x: Tensor) -> Tuple[Tensor, Tensor]:
        h = self.encoder(x)
        return self.z_mean_layer(h), self.z_std_layer(h)

    def reparameterize(self, mu: Tensor, logvar: Tensor) -> Tensor:
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        z = mu + eps * std
        return z

    def decode(self, z: Tensor) -> Tensor:
        # TODO Ogarnąć temat InverseTorusLayer
        # if self.torus_layer:
        #     z = self.inverse_z_layer(z)
        return self.decoder(z)

    def forward(self, x: Tensor) -> Tuple[Tensor, Tensor, Tensor]:
        mu, logvar = self.calc_mean_logvar(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

    def encode(self, x: Tensor) -> Tensor:
        mu, logvar = self.calc_mean_logvar(x)
        return mu

    def random_code(self, x: Tensor) -> Tensor:
        mu, logvar = self.calc_mean_logvar(x)
        z = self.reparameterize(mu, logvar)
        return z

    def sample(self, N: int):
        z = self.latent_sample(N)
        return self.decode(z.to(self.device()))

    def latent_sample(self, N: int) -> Tensor:
        return torch.randn([N, self.z_size])

    def train_epoch(self, dataset):
        for batch in dataset:
            self._training_step_vae(batch)

    def _training_step_vae(self, batch: Tensor):
        batch = batch.to(self.device())
        self.optimizer.zero_grad()
        output = self.forward(batch)
        loss = self.criterion(batch, output)
        loss.backward()
        self.optimizer.step()


class VAEL1(VAE):
    def __init__(
        self,
        encoder: nn.Module,
        decoder: nn.Module,
        beta: float,
        encoder_last_width: int,
        z_size: int,
        optimizer: torch.optim.Optimizer = None,
        lr: float = 0.001,
        torus_layer: bool = False,
    ):
        super().__init__(
            encoder, decoder, beta, encoder_last_width, z_size, optimizer, lr, torus_layer,
        )
        self.z_mse_criterion = L1ReconstructionLoss1D()

    def _training_step_vae(self, batch: Tensor):
        batch = batch.to(self.device())
        self.optimizer.zero_grad()
        output = self.forward(batch)
        loss = self.criterion(batch, output)
        loss.backward()
        self.optimizer.step()
        self.optimizer.zero_grad()
        z_sample = self.latent_sample(batch.shape[0]).to(self.device())
        output = self.encode(self.decode(z_sample))
        loss = self.z_mse_criterion(z_sample, output)
        loss.backward()
        self.optimizer.step()


class VAEA(VAE):
    def __init__(
        self,
        encoder: nn.Module,
        decoder: nn.Module,
        beta: float,
        encoder_last_width: int,
        z_size: int,
        optimizer: torch.optim.Optimizer = None,
        lr: float = 0.001,
        torus_layer: bool = False,
    ):
        super().__init__(
            encoder, decoder, beta, encoder_last_width, z_size, optimizer, lr, torus_layer,
        )
        self.z_mse_criterion = MSEReconstructionLoss1D()
        self.mask_layer = nn.Linear(1, z_size, bias=False)
        self.optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        self.mask_optimizer = torch.optim.Adam(self.mask_layer.parameters(), lr=self.lr)

    def _calculate_mask(self, N):
        mask_input = torch.ones(N, 1).to(self.device())
        return torch.sigmoid(self.mask_layer(mask_input))

    def calc_mean_logvar(self, x: Tensor) -> Tuple[Tensor, Tensor]:
        h = self.encoder(x)
        mean = self.z_mean_layer(h)
        logvar = self.z_std_layer(h)
        mask = self._calculate_mask(mean.shape[0])
        mean = mask * mean
        logvar = mask * logvar
        return mean, logvar

    def latent_sample(self, N: int) -> Tensor:
        mask = self._calculate_mask(N).cpu()
        return mask * torch.randn([N, self.z_size])

    def _training_step_vae(self, batch: Tensor):
        batch = batch.to(self.device())
        self.optimizer.zero_grad()
        output = self.forward(batch)
        loss = self.criterion(batch, output)
        loss.backward()
        self.optimizer.step()
        # self.mask_optimizer.zero_grad()
        self.optimizer.zero_grad()
        z_sample = self.latent_sample(batch.shape[0]).to(self.device())
        z_sample_distorted = z_sample + torch.normal(0.0, 0.0, z_sample.shape).to(self.device())
        output = self.encode(self.decode(z_sample_distorted))
        loss = self.z_mse_criterion(z_sample, output)
        mask = self._calculate_mask(1)
        # print(torch.round(mask), mask)
        loss += 0.1 * torch.abs(torch.round(mask) - mask).mean()
        loss.backward()
        # self.mask_optimizer.step()
        # print(self._calculate_mask(1))
        self.optimizer.step()


class VAEC(VAE):
    def __init__(
        self,
        encoder: nn.Module,
        decoder: nn.Module,
        beta: float,
        encoder_last_width: int,
        z_size: int,
        optimizer: torch.optim.Optimizer = None,
        lr: float = 0.001,
        torus_layer: bool = False,
        continuity_loss_sigma: float = 0.1,
        continuity_loss_weight: float = 1.0,
    ):
        super().__init__(
            encoder, decoder, beta, encoder_last_width, z_size, optimizer=optimizer, lr=lr, torus_layer=torus_layer,
        )
        self.continuity_loss_sigma = continuity_loss_sigma
        self.continuity_loss_weight = continuity_loss_weight

    def _continuity_loss(self, batch_size, n_group):
        corrs = []
        for i in range(batch_size // n_group):
            latent_sample = self.latent_sample(1).to(self.device())
            latent_sample = torch.cat([latent_sample] * n_group, 0)
            latent_diff = (torch.randn_like(latent_sample) * self.continuity_loss_sigma).to(self.device())
            latent_sample_shifted = latent_sample + latent_diff
            decoded_sample = self.decode(latent_sample)
            decoded_sample_shifted = self.decode(latent_sample_shifted)
            n_dim = len(decoded_sample.shape)
            dims = list(range(1, n_dim))
            diff_latent = ((latent_sample_shifted - latent_sample) ** 2).mean(1)
            diff_decoded = ((decoded_sample_shifted - decoded_sample) ** 2).mean(dims)
            corr = correlation(diff_decoded, diff_latent)
            corrs.append(corr)
        corr_mean = torch.FloatTensor(corrs).mean()
        loss_logger.add("VAELoss_corr", corr_mean.item())
        return corr_mean

    def _training_step_vae(self, batch: Tensor):
        batch = batch.to(self.device())
        self.optimizer.zero_grad()
        output = self.forward(batch)
        loss = self.criterion(batch, output)
        corr = self._continuity_loss(500, 10)
        loss += self.continuity_loss_weight * (1.0 - corr)
        loss.backward()
        self.optimizer.step()


class GANE(GenerativeModel):
    def __init__(
        self,
        encoder: nn.Module,
        decoder: nn.Module,
        discriminator: nn.Module,
        latent_generator: Callable,
        optimizer_encoder: torch.optim.Optimizer = None,
        optimizer_decoder: torch.optim.Optimizer = None,
        optimizer_discriminator: torch.optim.Optimizer = None,
        lr: float = 0.001,
        k: int = 1,
        encoder_weight: float = 1.0,
    ):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.discriminator = discriminator
        if optimizer_encoder:
            self.optimizer_encoder = optimizer_encoder
        else:
            self.optimizer_encoder = torch.optim.Adam(encoder.parameters(), lr=lr)
        if optimizer_decoder:
            self.optimizer_decoder = optimizer_decoder
        else:
            self.optimizer_decoder = torch.optim.Adam(decoder.parameters(), lr=lr)
        if optimizer_discriminator:
            self.optimizer_discriminator = optimizer_discriminator
        else:
            self.optimizer_discriminator = torch.optim.Adam(discriminator.parameters(), lr=lr)
        self.criterion_encoder = MSEReconstructionLoss1D()
        self.criterion_decoder = GANGeneratorCELoss()
        self.criterion_discriminator = GANDiscriminatorCELoss()
        self.latent_generator = latent_generator
        self.lr = lr
        self.k = k
        self.encoder_weight = encoder_weight

    def encode(self, x: Tensor) -> Tensor:
        return self.encoder(x)

    def decode(self, z: Tensor) -> Tensor:
        return self.decoder(z)

    def sample(self, N: int) -> Tensor:
        z = self.latent_sample(N)
        return self.decode(z.to(self.device()))

    def latent_sample(self, N: int) -> Tensor:
        return self.latent_generator(N).to(self.device())

    def train_epoch(self, dataset: Tensor):
        for i, batch in enumerate(dataset):
            batch = batch.to(self.device())
            if (i + 1) % (self.k + 1) == 0:
                self._discriminator_step(batch)
            else:
                self._encoder_decoder_step(batch)

    def _discriminator_step(self, batch: Tensor):
        self.optimizer_discriminator.zero_grad()
        g_output = self.sample(batch.shape[0])
        full_batch = torch.cat([batch, g_output])
        full_y = torch.LongTensor([1] * batch.shape[0] + [0] * g_output.shape[0])
        full_y = full_y.to(self.device())
        full_preds = self.discriminator(full_batch)
        loss = self.criterion_discriminator(full_preds, full_y)
        auc_logger.add(
            "GAN_Discriminator_AUC",
            roc_auc_score(full_y.cpu().detach().numpy(), full_preds.cpu().detach().numpy()[:, 1]),
        )
        loss.backward()
        self.optimizer_discriminator.step()

    def _encoder_decoder_step(self, batch: Tensor):
        self.optimizer_encoder.zero_grad()
        self.optimizer_decoder.zero_grad()
        noise = self.latent_sample(batch.shape[0])
        g_output = self.decode(noise)
        e_output = self.encode(g_output)
        full_batch = torch.cat([batch, g_output])
        full_y = torch.LongTensor([0] * batch.shape[0] + [1] * g_output.shape[0])
        full_y = full_y.to(self.device())
        full_preds = self.discriminator(full_batch)
        loss_g = self.criterion_decoder(full_preds, full_y)
        loss_e = self.criterion_encoder(e_output, noise)
        loss = loss_g + self.encoder_weight * loss_e
        loss.backward()
        self.optimizer_decoder.step()
        self.optimizer_encoder.step()
