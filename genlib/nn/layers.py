import torch
from torch import nn, Tensor


class TorusLayer(nn.Module):
    def __init__(self, input_size: int, output_size: int):
        super().__init__()
        self.x1_layer = nn.Linear(input_size, output_size)
        self.x2_layer = nn.Linear(input_size, output_size)

    def forward(self, x: Tensor) -> Tensor:
        x1 = self.x1_layer(x)
        x2 = self.x2_layer(x)
        r = (x1 ** 2 + x2 ** 2 + 0.000001) ** (1 / 2)
        cos_fi = x1 / r
        return torch.acos(cos_fi) * torch.sign(x2)


class InverseTorusLayer(nn.Module):
    def __init__(self, input_size: int, output_size: int):
        super().__init__()
        self.layer = nn.Linear(2 * input_size, output_size)

    def forward(self, x: Tensor) -> Tensor:
        x = torch.cat([torch.cos(x), torch.sin(x)], 1)
        x = self.layer(x)
        return x
