from typing import Tuple

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor

from genlib.loggers import loss_logger


class MSEReconstructionLoss1D(nn.Module):
    def forward(self, reconstruction: Tensor, input_: Tensor) -> Tensor:
        loss = F.mse_loss(reconstruction, input_, reduction="mean")
        loss_logger.add("MSE_reconstruction_loss", loss.item())
        return loss


class VAELoss(nn.Module):
    def __init__(self, reconstruction_loss: nn.Module, beta: float = 1.0):
        super().__init__()
        self.reconstruction_loss = reconstruction_loss
        self.beta = beta

    def forward(self, vae_input: Tensor, vae_output: Tuple[Tensor, Tensor, Tensor]) -> Tensor:
        reconstruction, mean, logvar = vae_output
        reconstruction_loss = self.reconstruction_loss(reconstruction, vae_input)
        kld = -0.5 * torch.mean(1 + logvar - mean.pow(2) - logvar.exp())
        loss_logger.add("VAELoss_reconstruction_loss", reconstruction_loss.item())
        loss_logger.add("VAELoss_kld", kld.item())
        return reconstruction_loss + self.beta * kld


class AELoss(nn.Module):
    def __init__(self, reconstruction_loss: nn.Module, dist_weight: float = 1.0):
        super().__init__()
        self.reconstruction_loss = reconstruction_loss
        self.dist_weight = dist_weight

    def forward(self, ae_input: Tensor, ae_output: Tuple[Tensor, Tensor]) -> Tensor:
        reconstruction, z = ae_output
        reconstruction_loss = self.reconstruction_loss(reconstruction, ae_input)
        loss_logger.add("AELoss_reconstruction_loss", reconstruction_loss.item())
        return reconstruction_loss


class GANDiscriminatorCELoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.ce_loss = torch.nn.CrossEntropyLoss()

    def forward(self, prediction: Tensor, target: Tensor) -> Tensor:
        loss = self.ce_loss(prediction, target)
        loss_logger.add("GAN_discriminator_loss", loss.item())
        return loss


class GANGeneratorCELoss(GANDiscriminatorCELoss):
    def forward(self, prediction: Tensor, target: Tensor) -> Tensor:
        loss = self.ce_loss(prediction, target)
        loss_logger.add("GAN_generator_loss", loss.item())
        return loss


class L1ReconstructionLoss1D(nn.Module):
    def forward(self, reconstruction: Tensor, input_: Tensor) -> Tensor:
        loss = torch.mean(torch.abs(reconstruction - input_))
        loss_logger.add("L1_reconstruction_loss", loss.item())
        return loss
