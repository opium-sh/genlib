from abc import ABC
from typing import Tuple, Iterable

import torch
import torch.nn as nn
from torch import Tensor
from torch.nn import ModuleList


class CNN(nn.Module, ABC):
    pass


class MLP(nn.Module, ABC):
    pass


class MLPEncoder(MLP):
    def __init__(
        self,
        input_size: int,
        layers_sizes: Tuple[int] = (100,),
        activation: nn.Module = nn.ReLU,
        dropout: float = None,
        batchnorm: bool = False,
        latent_size: int = None,
    ):
        super().__init__()
        if dropout:
            assert 0.0 < dropout < 1.0
        layers_sizes = [input_size] + list(layers_sizes)
        self.layers = ModuleList()
        for i in range(1, len(layers_sizes)):
            self.layers.append(nn.Linear(layers_sizes[i - 1], layers_sizes[i]))
            if batchnorm:
                self.layers.append(nn.BatchNorm1d(layers_sizes[i]))
            self.layers.append(activation)
            if dropout:
                self.layers.append(nn.Dropout(dropout))
        if latent_size:
            self.layers.append(nn.Linear(layers_sizes[-1], latent_size))
        self.encoder = nn.Sequential(*self.layers)

    def forward(self, x: Tensor) -> Tensor:
        x = x.reshape(x.shape[0], -1)
        return self.encoder(x)


class MLPDecoder(MLP):
    def __init__(
        self,
        latent_size: int,
        output_size: int,
        layers_sizes: Tuple[int] = (100,),
        activation: nn.Module = nn.ReLU,
        dropout: float = None,
        batchnorm: bool = False,
        image_size: Tuple[int, int] = None,
    ):
        super().__init__()
        if dropout:
            assert 0.0 < dropout < 1.0
        layers_sizes = [latent_size] + list(layers_sizes) + [output_size]
        self.layers = ModuleList()
        for i in range(1, len(layers_sizes)):
            self.layers.append(nn.Linear(layers_sizes[i - 1], layers_sizes[i]))
            if i < (len(layers_sizes) - 1):
                if batchnorm:
                    self.layers.append(nn.BatchNorm1d(layers_sizes[i]))
                self.layers.append(activation)
                if dropout:
                    self.layers.append(nn.Dropout(dropout))
        self.encoder = nn.Sequential(*self.layers)
        self.image_size = image_size

    def forward(self, x: Tensor) -> Tensor:
        x = self.encoder(x)
        if self.image_size is not None:
            bs = x.shape[0]
            x = x.reshape([bs] + list(self.image_size))
        return x


class MLPDiscriminator(MLP):
    def __init__(
        self,
        input_size: int,
        layers_sizes: Tuple[int] = (100,),
        activation: nn.Module = nn.ReLU,
        dropout: float = None,
        batchnorm: bool = False,
    ):
        super().__init__()
        if dropout:
            assert 0.0 < dropout < 1.0
        layers_sizes = [input_size] + list(layers_sizes)
        self.layers = ModuleList()
        for i in range(1, len(layers_sizes)):
            self.layers.append(nn.Linear(layers_sizes[i - 1], layers_sizes[i]))
            if batchnorm:
                self.layers.append(nn.BatchNorm1d(layers_sizes[i]))
            self.layers.append(activation)
            if dropout:
                self.layers.append(nn.Dropout(dropout))
        self.layers.append(nn.Linear(layers_sizes[-1], 2))
        self.model = nn.Sequential(*self.layers)

    def forward(self, x: Tensor) -> Tensor:
        x = x.reshape(x.shape[0], -1)
        return self.model(x)


class CNNEncoder(CNN):
    def __init__(
        self,
        input_size: Tuple[int, int, int],
        n_filters: Iterable[int] = (32, 32),
        stride: int = 1,
        kernel_size: int = 3,
        activation: nn.Module = nn.ReLU(),
        dropout: float = None,
        batchnorm: bool = False,
        latent_size: int = None,
    ):
        super().__init__()
        self.input_size = input_size
        if dropout:
            assert 0.0 < dropout < 1.0
        self.layers = ModuleList()
        n_filters = [input_size[0]] + list(n_filters)
        for i in range(1, len(n_filters)):
            self.layers.append(nn.Conv2d(n_filters[i - 1], n_filters[i], kernel_size, stride=stride))
            if batchnorm:
                raise NotImplementedError()
            self.layers.append(activation)
            if dropout:
                self.layers.append(nn.Dropout2d(dropout))
        self.encoder = nn.Sequential(*self.layers)
        self.latent_size = latent_size
        self.cnn_out_shape = self.encoder(torch.ones((1,) + input_size))[0].shape
        self.cnn_out_size = torch.prod(torch.FloatTensor(list(self.cnn_out_shape))).long().item()
        if latent_size:
            self.latent_layer = nn.Linear(torch.prod(self.cnn_out_shape), latent_size)
        else:
            self.latent_layer = None

    def forward(self, x: Tensor) -> Tensor:
        x = self.encoder(x)
        bs = x.shape[0]
        x = x.reshape(bs, -1)
        if self.latent_layer:
            x = self.latent_layer(x)
        return x


class CNNDecoder(CNN):
    def __init__(
        self,
        latent_size: int,
        first_layer_shape: Tuple[int, int, int],
        output_shape: Tuple[int, int, int],
        n_filters: Iterable[int] = (32, 32),
        stride: int = 1,
        kernel_size: int = 3,
        activation: nn.Module = nn.ReLU,
        dropout: float = None,
        batchnorm: bool = False,
    ):
        super().__init__()
        if dropout:
            assert 0.0 < dropout < 1.0
        assert first_layer_shape[0] == n_filters[0], "Number of filters have to match"
        self.first_layer_shape = first_layer_shape
        self.layers = ModuleList()
        n_filters = list(n_filters) + [output_shape[0]]
        fl_size = torch.prod(torch.FloatTensor(list(first_layer_shape))).long().item()
        self.first_layer = nn.Linear(latent_size, fl_size)
        for i in range(1, len(n_filters)):
            self.layers.append(nn.ConvTranspose2d(n_filters[i - 1], n_filters[i], kernel_size, stride=stride))
            if batchnorm:
                raise NotImplementedError()
            if i < len(n_filters) - 1:
                self.layers.append(activation)
            if dropout:
                self.layers.append(nn.Dropout2d(dropout))
        self.decoder = nn.Sequential(*self.layers)
        self.latent_size = latent_size

    def forward(self, x: Tensor) -> Tensor:
        bs = x.shape[0]
        x = self.first_layer(x)
        x = x.reshape([bs] + list(self.first_layer_shape))
        return self.decoder(x)


class CNNDiscriminator(CNN):
    def __init__(
        self,
        input_size: Tuple[int, int, int],
        n_filters: Iterable[int] = (32, 32),
        stride: int = 1,
        kernel_size: int = 3,
        last_hidden_size: int = 100,
        activation: nn.Module = nn.ReLU(),
        dropout: float = None,
        batchnorm: bool = False,
    ):
        super().__init__()
        self.input_size = input_size
        if dropout:
            assert 0.0 < dropout < 1.0
        self.layers = ModuleList()
        n_filters = [input_size[0]] + list(n_filters)
        for i in range(1, len(n_filters)):
            self.layers.append(nn.Conv2d(n_filters[i - 1], n_filters[i], kernel_size, stride=stride))
            if batchnorm:
                raise NotImplementedError()
            self.layers.append(activation)
            if dropout:
                self.layers.append(nn.Dropout2d(dropout))
        self.convolutions = nn.Sequential(*self.layers)
        self.cnn_out_shape = self.convolutions(torch.ones((1,) + input_size))[0].shape
        self.cnn_out_size = torch.prod(torch.FloatTensor(list(self.cnn_out_shape))).long().item()
        self.last_hidden_layer = nn.Linear(torch.prod(self.cnn_out_shape), last_hidden_size)
        self.classifier = nn.Linear(last_hidden_size, 2)

    def forward(self, x: Tensor) -> Tensor:
        x = self.convolutions(x)
        bs = x.shape[0]
        x = x.reshape(bs, -1)
        x = self.activation(self.last_hidden_layer(x))
        return self.classifier(x)
