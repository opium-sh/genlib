from genlib.nn.architectures import VAE, GANE, VAEC, VAEA, VAEL1
from genlib.nn.components import (
    MLPEncoder,
    MLPDecoder,
    MLPDiscriminator,
    CNNEncoder,
    CNNDecoder,
)


def vae_model(
    input_size,
    layers_sizes,
    activation,
    dropout,
    batchnorm,
    latent_size,
    beta,
    lr,
    torus_layer=False,
    image_size=None,
    device="cuda:0",
):
    encoder = MLPEncoder(input_size, layers_sizes, activation=activation, dropout=dropout, batchnorm=batchnorm).to(
        device
    )
    decoder = MLPDecoder(
        latent_size,
        input_size,
        layers_sizes[::-1],
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
        image_size=image_size,
    ).to(device)
    return VAE(encoder, decoder, beta, layers_sizes[-1], latent_size, lr=lr, torus_layer=torus_layer).to(device)


def vaea_model(
    input_size,
    layers_sizes,
    activation,
    dropout,
    batchnorm,
    latent_size,
    beta,
    lr,
    torus_layer=False,
    image_size=None,
    device="cuda:0",
):
    encoder = MLPEncoder(input_size, layers_sizes, activation=activation, dropout=dropout, batchnorm=batchnorm).to(
        device
    )
    decoder = MLPDecoder(
        latent_size,
        input_size,
        layers_sizes[::-1],
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
        image_size=image_size,
    ).to(device)
    return VAEA(encoder, decoder, beta, layers_sizes[-1], latent_size, lr=lr, torus_layer=torus_layer).to(device)


def vael1_model(
    input_size,
    layers_sizes,
    activation,
    dropout,
    batchnorm,
    latent_size,
    beta,
    lr,
    torus_layer=False,
    image_size=None,
    device="cuda:0",
):
    encoder = MLPEncoder(input_size, layers_sizes, activation=activation, dropout=dropout, batchnorm=batchnorm).to(
        device
    )
    decoder = MLPDecoder(
        latent_size,
        input_size,
        layers_sizes[::-1],
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
        image_size=image_size,
    ).to(device)
    return VAEL1(encoder, decoder, beta, layers_sizes[-1], latent_size, lr=lr, torus_layer=torus_layer).to(device)


def vaec_model(
    input_size,
    layers_sizes,
    activation,
    dropout,
    batchnorm,
    latent_size,
    dist_weight,
    lr,
    torus_layer=False,
    image_size=None,
    continuity_loss_sigma=0.1,
    continuity_loss_weight=1.0,
    device="cuda:0",
):
    encoder = MLPEncoder(input_size, layers_sizes, activation=activation, dropout=dropout, batchnorm=batchnorm,).to(
        device
    )
    decoder = MLPDecoder(
        latent_size,
        input_size,
        layers_sizes[::-1],
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
        image_size=image_size,
    ).to(device)
    return VAEC(
        encoder,
        decoder,
        dist_weight,
        layers_sizes[-1],
        latent_size,
        lr=lr,
        torus_layer=torus_layer,
        continuity_loss_sigma=continuity_loss_sigma,
        continuity_loss_weight=continuity_loss_weight,
    ).to(device)


def vae_cnn_model(
    image_size,
    n_filters,
    stride,
    activation,
    dropout,
    batchnorm,
    latent_size,
    dist_weight,
    lr,
    torus_layer=False,
    device="cuda:0",
):
    encoder = CNNEncoder(
        image_size, n_filters, stride=stride, activation=activation, dropout=dropout, batchnorm=batchnorm,
    ).to(device)
    decoder = CNNDecoder(
        latent_size,
        encoder.cnn_out_shape,
        image_size,
        n_filters[::-1],
        stride=stride,
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
    ).to(device)
    return VAE(encoder, decoder, dist_weight, encoder.cnn_out_size, latent_size, lr=lr, torus_layer=torus_layer,).to(
        device
    )


def gane_model(
    input_size,
    latent_size,
    latent_generator,
    layers_sizes_g,
    layers_sizes_d,
    activation,
    dropout,
    batchnorm,
    lr,
    image_size=None,
    encoder_weight=1.0,
    k=1,
    device="cuda:0",
):
    decoder = MLPDecoder(
        latent_size,
        input_size,
        layers_sizes_g,
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
        image_size=image_size,
    ).to(device)
    discriminator = MLPDiscriminator(
        input_size, layers_sizes_d, activation=activation, dropout=dropout, batchnorm=batchnorm,
    ).to(device)
    encoder = MLPEncoder(
        input_size,
        layers_sizes_g[::-1],
        activation=activation,
        dropout=dropout,
        batchnorm=batchnorm,
        latent_size=latent_size,
    ).to(device)
    return GANE(encoder, decoder, discriminator, latent_generator, lr=lr, encoder_weight=encoder_weight, k=k,)
