from typing import Tuple, Sized

from torch.utils.data import DataLoader


def split_dataset(data: Sized, train_split: float, val_split: float = None, batch_size: int = 32) -> Tuple:
    if val_split:
        split1 = int(len(data) * train_split)
        split2 = int(len(data) * val_split)
        return (
            DataLoader(data[:split1], batch_size=batch_size),
            DataLoader(data[split1:split2], batch_size=batch_size),
            DataLoader(data[split2:], batch_size=batch_size),
        )
    else:
        split = int(len(data) * train_split)
        return (
            DataLoader(data[:split], batch_size=batch_size),
            DataLoader(data[split:], batch_size=batch_size),
        )
