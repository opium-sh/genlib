from genlib.loggers import loss_logger
from genlib.plots import plot_model
from genlib.training import split_dataset


def experiment(model, theta_generator, ds_generator, N, batch_size, n_epochs, figsize=(30, 20), hist_log_scale=False, plot_scatter_matrix=True):
    theta = theta_generator(N)
    x = ds_generator(theta)
    train_data, val_data = split_dataset(x, 1.0, batch_size=batch_size)
    loss_logger.clear()
    model.train_model(train_data, n_epochs)
    plot_model(model, theta, x, s=10, N=N, bins=50, figsize=figsize, hist_log_scale=hist_log_scale, plot_scatter_matrix=plot_scatter_matrix)
    return model, theta, x
