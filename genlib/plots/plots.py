import matplotlib.pyplot as plt
import numpy as np
import torch

from genlib.plots.subplots import (
    plot_model_loss,
    data_visualization,
    plot_recon_f,
    generate_bins,
    theta_plot,
    multidimensional_plot,
)


def l2_dist(x, y):
    dist = (x - y) ** 2
    dist = dist.sum(axis=tuple(range(1, dist.ndim)))
    dist = np.sqrt(dist)
    return dist


def plot_model(
    model, theta, x_data, figsize=(30, 20), s=3, alpha=0.3, N=1000, bins=10, hist_log_scale=False, plot_scatter_matrix=True,
):
    height = 5
    width = 5

    device = model.device()

    model.eval()

    with torch.no_grad():
        x_enc_dec = model.decode(model.encode(x_data.to(device))).cpu().detach().numpy()
        l_gen = model.latent_sample(N)
        x_gen = model.decode(l_gen.to(device)).cpu().detach().numpy()
        l_data = model.encode(x_data.to(device)).cpu().detach().numpy()
        l_dec_enc = model.encode(model.decode(l_gen.to(device))).cpu().detach().numpy()
        x_data = x_data.numpy()
        l_gen = l_gen.cpu().numpy()
    max_data_width = x_data.max() - x_data.min()
    data_min_max = [x_data.min(), x_data.max()]

    plt.figure(figsize=figsize)

    "Losses"
    plot_model_loss(height, width, model)

    bins_latent = generate_bins(l_gen, l_dec_enc, l_data, bins)
    bins_data = generate_bins(x_data, x_enc_dec, x_gen, bins)

    title = "Original data space distribution"
    plt.subplot(height, width, width + 1)
    data_visualization(x_data, None, bins_data, None, None, title, data_min_max, hist_log_scale)

    title = "Reconstructed data space distribution"
    plt.subplot(height, width, width + 2)
    data_visualization(
        x_data,
        x_enc_dec,
        bins_data,
        "Original data distribution",
        "Encoded-decoded data distribution",
        title,
        data_min_max,
        hist_log_scale,
    )

    title = "Sample data generated from model"
    plt.subplot(height, width, width + 3)
    data_visualization(
        x_data,
        x_gen,
        bins_data,
        "Original data distribution",
        "Sampled data distribution",
        title,
        data_min_max,
        hist_log_scale,
    )

    title = "Data reconstruction field"
    ax = plt.subplot(height, width, width + 4)
    plot_recon_f(N, x_data, x_enc_dec, alpha, s, ax, "Original Data", "Encoded-Decoded Data", title)

    title = "Histogram of distances from data to end-dec data"
    plt.subplot(height, width, 1 * width + 5)
    dist = l2_dist(x_data, x_enc_dec)
    plt.hist(dist, bins=bins, density=True)
    plt.plot([max_data_width, max_data_width], [0, 1], c="r")
    plt.title(title)

    title = "Original latent space distribution"
    plt.subplot(height, width, 2 * width + 1)
    data_visualization(l_gen, None, bins_latent, None, None, title, data_min_max, hist_log_scale)

    title = "Encoded data distribution"
    plt.subplot(height, width, 2 * width + 2)
    data_visualization(
        l_gen,
        l_data,
        bins_latent,
        "Original latent distribution",
        "Encoded data distribution",
        title,
        data_min_max,
        hist_log_scale,
    )

    title = "Decoded-encoded latent distribution"
    plt.subplot(height, width, 2 * width + 3)
    data_visualization(
        l_gen,
        l_dec_enc,
        bins_latent,
        "Original latent distribtion",
        "Decoded-encoded latent distribution",
        title,
        data_min_max,
        hist_log_scale,
    )

    title = "Latent space reconstruction field"
    ax = plt.subplot(height, width, 2 * width + 4)
    plot_recon_f(
        N, l_gen, l_dec_enc, alpha, s, ax, "Original latent", "Decoded-encoded latent", title,
    )

    title = "Histogram of distances from latent sample to end-dec sample"
    plt.subplot(height, width, 2 * width + 5)
    dist = l2_dist(l_gen, l_dec_enc)
    plt.hist(dist, bins=bins, density=True)
    plt.plot([3, 3], [0, 1], c="r")
    plt.title(title)

    if theta is not None:

        title = "Theta distribution"
        plt.subplot(height, width, 3 * width + 1)
        plt.hist(theta, bins=bins, density=True)
        plt.title(title)

        title = "Theta vs latent Space"
        plt.subplot(height, width, 3 * width + 2)
        theta_plot(theta, l_data, s, cmap="viridis", title=title)

    title = "Latent space distance vs data space distance"
    plt.subplot(height, width, 4 * width + 1)
    l_dist = l2_dist(l_gen[:-1], l_gen[1:])
    x_dist = l2_dist(x_gen[:-1], x_gen[1:])
    plt.scatter(l_dist, x_dist, s=s, alpha=0.1)
    plt.title(title)

    title = "Data space distance divided by latent space distance"
    plt.subplot(height, width, 4 * width + 2)
    l_dist = l2_dist(l_gen[:-1], l_gen[1:])
    x_dist = l2_dist(x_gen[:-1], x_gen[1:])
    plt.hist(x_dist / (l_dist + 0.00001), bins=bins)
    plt.title(title)

    plt.show()
    if plot_scatter_matrix:
        fig = plt.figure(figsize=(12, 12))
        outer_ax = plt.subplot(1, 1, 1)
        multidimensional_plot(l_gen, l_data, l_dec_enc, outer_ax, fig, N, s=3, alpha=alpha)
        plt.show()
