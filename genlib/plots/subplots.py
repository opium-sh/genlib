import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np

from genlib.loggers import loss_logger, auc_logger
from genlib.nn.architectures import VAE, GANE, VAEC, VAEA


def image_cat(x, n):
    return np.concatenate([np.concatenate([x[i + j, 0] for i in range(n)], 1) for j in range(n)])


def generate_bins(x_orig, x_transformed, x_gen, bins):
    x = np.concatenate([x_orig, x_transformed, x_gen])
    min_0 = x[:, 0].min()
    max_0 = x[:, 0].max()
    if x_orig.shape[1] == 1:
        bins_data = np.linspace(min_0 - 0.05 * np.abs(min_0), max_0 + 0.05 * np.abs(max_0), bins)
    elif x_orig.shape[1] == 2:
        min_1 = x[:, 1].min()
        max_1 = x[:, 1].max()
        bins_data = [
            np.linspace(min_0 - 0.05 * np.abs(min_0), max_0 + 0.05 * np.abs(max_0), bins),
            np.linspace(min_1 - 0.05 * np.abs(min_1), max_1 + 0.05 * np.abs(max_1), bins),
        ]
    else:
        bins_data = None
    return bins_data


def plot_loss(name, ylim=None, log=False, linewidth=1, title=None):
    loss = loss_logger.get_data()[name]
    plt.plot(loss, linewidth=linewidth)
    if not title:
        plt.title(name)
    else:
        plt.title(title)
    if ylim:
        plt.ylim(*ylim)
    if log:
        plt.yscale("log")


def plot_auc(name, ylim=(-0.05, 1.05), linewidth=1):
    auc = auc_logger.get_data()[name]
    plt.plot(auc, linewidth=linewidth)
    plt.plot(np.zeros_like(auc) + 0.5, c="r", alpha=0.5)
    plt.title(name)
    plt.ylim(*ylim)


def plot_model_loss(height, width, model):
    if isinstance(model, VAE):
        rec_loss = loss_logger.get_data()["VAELoss_reconstruction_loss"]
        kld = loss_logger.get_data()["VAELoss_kld"]

        plt.subplot(height, width, 1)
        plot_loss("VAELoss_reconstruction_loss", log=True)

        plt.subplot(height, width, 2)
        plot_loss("VAELoss_kld")

        plt.subplot(height, width, 3)
        plt.plot(model.beta * np.array(kld) / np.array(rec_loss), linewidth=1)
        plt.title("kld/reconstuction_loss")

        if isinstance(model, VAEC):
            plt.subplot(height, width, 4)
            plot_loss("VAELoss_corr", log=True)

        if isinstance(model, VAEA):
            plt.subplot(height, width, 4)
            plot_loss("MSE_reconstruction_loss", log=True, title="MSE_rec_loss_in_Z_space")

    elif isinstance(model, GANE):
        plt.subplot(height, width, 1)
        plot_loss("GAN_discriminator_loss")

        plt.subplot(height, width, 2)
        plot_loss("GAN_generator_loss")

        plt.subplot(height, width, 3)
        plot_loss("MSE_reconstruction_loss", log=True)

        plt.subplot(height, width, 4)
        plot_auc("GAN_Discriminator_AUC", ylim=(0, 1.05))
    else:
        raise Exception("Wrong model_type, must be in ('GANE', 'VAE, 'VAEC', 'VAEA')")


def plot_reconstruction_field(ax, x_orig, x_transformed, color="r", alpha=0.2):
    for (x1, y1), (x2, y2) in zip(x_orig, x_transformed):
        ax.plot([x1, x2], [y1, y2], c=color, alpha=alpha)


def plot_2d_recon_f(N, x_orig, x_transformed, alpha, s, ax, label_orig, label_transformed, title):
    sparse_skip = N // 200
    x_orig_plot = x_orig[::sparse_skip, :]
    x_transformed_plot = x_transformed[::sparse_skip, :]
    plt.scatter(x_orig_plot[:, 0], x_orig_plot[:, 1], s=s, c="b", alpha=alpha, label=label_orig)
    plot_reconstruction_field(ax, x_orig_plot, x_transformed_plot)
    plt.scatter(
        x_transformed_plot[:, 0], x_transformed_plot[:, 1], s=s, c="orange", alpha=alpha, label=label_transformed,
    )
    plt.title(title)
    plt.legend()


def plot_1d_recon_f(N, x_orig, x_transformed, alpha, s, ax, label_orig, label_transformed, title):
    sparse_skip = N // 200
    x_orig_plot = x_orig[::sparse_skip]
    x_orig_plot = np.concatenate([x_orig_plot, np.zeros_like(x_orig_plot)], 1)
    x_transformed_plot = x_transformed[::sparse_skip]
    x_transformed_plot = np.concatenate([x_transformed_plot, np.ones_like(x_transformed_plot)], 1)
    plot_reconstruction_field(ax, x_orig_plot, x_transformed_plot)
    plt.scatter(
        x_transformed_plot[:, 0], x_transformed_plot[:, 1], s=s, c="orange", alpha=alpha, label=label_transformed,
    )
    plt.scatter(
        x_orig_plot[:, 0], x_orig_plot[:, 1], s=s, c="blue", alpha=alpha, label=label_orig,
    )
    plt.title(title)
    plt.legend()


def plot_recon_f(N, x_orig, x_transformed, alpha, s, ax, label_orig, label_transformed, title):
    assert x_orig.shape == x_transformed.shape
    if len(x_orig.shape) == 2:
        input_size = x_orig.shape[1]
        if input_size == 1:
            plot_1d_recon_f(
                N, x_orig, x_transformed, alpha, s, ax, label_orig, label_transformed, title,
            )
        elif input_size == 2:
            plot_2d_recon_f(
                N, x_orig, x_transformed, alpha, s, ax, label_orig, label_transformed, title,
            )
        else:
            pass
    elif len(x_orig.shape) == 3:
        pass


def hist_1d(x_orig, x_transformed, bins, label_orig, label_transformed, title):
    plt.hist(x_orig[:, 0], bins=bins, alpha=0.5, density=True, label=label_orig)
    plt.hist(x_transformed[:, 0], bins=bins, alpha=0.5, density=True, label=label_transformed)
    plt.title(title)
    plt.legend()


def hist_2d(x, bins, title, log_scale: bool):
    if log_scale:
        plt.hist2d(x[:, 0], x[:, 1], bins=bins, norm=mpl.colors.LogNorm())
    else:
        plt.hist2d(x[:, 0], x[:, 1], bins=bins)
    plt.colorbar()
    plt.title(title)


def vector_data_visualisation(
    x_orig, x_transformed, bins, label_orig, label_transformed, title, data_min_max, log_scale,
):
    input_size = x_orig.shape[1]
    if x_transformed is not None:
        assert x_orig.shape == x_transformed.shape
        if input_size == 1:
            hist_1d(x_orig, x_transformed, bins, label_orig, label_transformed, title)
        elif input_size == 2:
            hist_2d(x_transformed, bins, title, log_scale=log_scale)
        else:
            pass
    else:
        if input_size == 1:
            plt.hist(x_orig[:, 0], bins=bins, density=True)
            plt.title(title)
        elif input_size == 2:
            hist_2d(x_orig, bins, title, log_scale=log_scale)
        else:
            pass


def image_data_visualization(
    x_orig, x_transformed, bins, label_orig, label_transformed, title, data_min_max,
):
    if x_transformed is not None:
        assert x_orig.shape == x_transformed.shape
        plt.matshow(image_cat(x_transformed, 3), 0, vmin=data_min_max[0], vmax=data_min_max[1])
        plt.colorbar()
        plt.title(title)
    else:
        plt.matshow(image_cat(x_orig, 3), 0)
        plt.colorbar()
        plt.title(title)


def data_visualization(x_orig, x_transformed, bins, label_orig, label_transformed, title, data_min_max, log_scale):
    if len(x_orig.shape) == 2:
        vector_data_visualisation(
            x_orig, x_transformed, bins, label_orig, label_transformed, title, data_min_max, log_scale
        )
    elif len(x_orig.shape) == 4:
        image_data_visualization(x_orig, x_transformed, bins, label_orig, label_transformed, title, data_min_max)


def theta_plot(theta, x, s, cmap, title):
    if len(x.shape) == 2:
        if x.shape[1] == 1:
            plt.scatter(theta, x, s=s)
            plt.title(title)
        elif x.shape[1] == 2:
            plt.scatter(x[:, 0], x[:, 1], s=s, c=theta, cmap=cmap)
            plt.colorbar()
            plt.title(title)
    elif len(x.shape) == 4:
        raise NotImplementedError()


def multidimensional_plot(original, enc, dec_enc, outer, fig, N, s, alpha):
    dim = original.shape[1]
    sparse_skip = N // 200
    original_plot = original[::sparse_skip, :]
    enc_plot = enc[::sparse_skip, :]
    dec_enc_plot = dec_enc[::sparse_skip, :]
    outer.set_xticks([])
    outer.set_yticks([])
    inner = gridspec.GridSpecFromSubplotSpec(dim, dim, subplot_spec=outer, wspace=0, hspace=0)
    stacked = np.concatenate((original_plot, enc_plot, dec_enc_plot))
    ranges = np.array((stacked.min(axis=0), stacked.max(axis=0))).T
    for col in range(dim):
        for row in range(dim):
            num = row * dim + col
            ax = plt.Subplot(fig, inner[num])
            if col > 0:
                ax.set_yticks([])
            if row < dim - 1:
                ax.set_xticks([])
            fig.add_subplot(ax)
            plt.xlim(ranges[col, 0], ranges[col, 1])
            bins = np.linspace(ranges[col, 0], ranges[col, 1], 50)
            if row == col:
                plt.hist(original[:, row], alpha=0.2, color="b", label="original", bins=bins)
                plt.hist(enc[:, row], alpha=0.2, color="r", label="enc", bins=bins)
                plt.hist(dec_enc[:, row], alpha=0.2, color="y", label="dec_enc", bins=bins)
                ax.set_yticks([])
            elif row > col:
                plt.scatter(original_plot[:, col], original_plot[:, row], s=2 * s, c="b", alpha=alpha / 2)
                plt.scatter(enc_plot[:, col], enc_plot[:, row], s=s, c="r", alpha=alpha)
                plt.ylim(ranges[row, 0], ranges[row, 1])
            else:
                plt.scatter(original_plot[:, col], original_plot[:, row], s=s, c="b", alpha=alpha)
                plt.scatter(dec_enc_plot[:, col], dec_enc_plot[:, row], s=s, c="y", alpha=alpha)
                plt.ylim(ranges[row, 0], ranges[row, 1])
                plot_reconstruction_field(ax, original_plot[:, [col, row]], dec_enc_plot[:, [col, row]], alpha=0.1)
    plt.legend()
    fig.show()
