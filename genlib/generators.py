import math

import numpy as np
import torch
from scipy.ndimage import gaussian_filter, rotate, shift

circle_ds = lambda theta: torch.stack([torch.sin(theta), torch.cos(theta)]).T
weird_circle_ds = lambda theta: torch.stack([torch.sin(theta) * torch.abs(theta), torch.cos(theta)]).T
spiral_ds = lambda theta: torch.stack([torch.sin(theta) * theta, torch.cos(theta) * theta]).T
half_circle_ds = lambda theta: torch.stack([torch.sin(theta / 2), torch.cos(theta / 2)]).T
quater_circle_ds = lambda theta: torch.stack([torch.sin(theta / 4), torch.cos(theta / 4)]).T
three_quater_circle_ds = lambda theta: torch.stack([torch.sin(theta * 3 / 4), torch.cos(theta * 3 / 4)]).T
line_ds = lambda theta: torch.stack([theta, theta]).T
identity_1D_ds = lambda x: x.reshape([x.shape[0], 1])
parabola_ds = lambda theta: torch.stack([theta, theta ** 2]).T


def generate_arrow_images_ds(theta):
    image = torch.zeros((11, 11))
    image[:, 5] = 1
    image[1, 4:7] = 1
    image[2, 3:8] = 1
    image = gaussian_filter(image, 0.5)
    return torch.stack(
        [
            torch.FloatTensor(rotate(image, alpha, reshape=False, prefilter=True, order=1)).reshape(1, 11, 11)
            for alpha in theta
        ]
    )


def generate_arrow_images_Z2D_ds(theta):
    image = torch.zeros((21, 21))
    image[6:14, 10] = 1
    image[7, 9:12] = 1
    image[8, 8:13] = 1
    image = gaussian_filter(image, 0.5)
    return torch.stack(
        [
            torch.FloatTensor(
                shift(rotate(image, alpha, reshape=False, prefilter=True, order=1,), (0, 2 * np.random.normal()),)
            ).reshape(1, 21, 21)
            for alpha in theta
        ]
    )


def generate_arrow_images_Z3D_ds(theta):
    image = torch.zeros((21, 21))
    image[6:14, 10] = 1
    image[7, 9:12] = 1
    image[8, 8:13] = 1
    image = gaussian_filter(image, 0.5)
    return torch.stack(
        [
            torch.FloatTensor(
                shift(
                    rotate(image, alpha, reshape=False, prefilter=True, order=1,),
                    (2 * np.random.normal(), 2 * np.random.normal()),
                )
            ).reshape(1, 21, 21)
            for alpha in theta
        ]
    )


pi_uniform_generator = lambda N: torch.rand(N) * 1 * math.pi
two_pi_uniform_generator = lambda N: torch.rand(N) * 2 * math.pi
three_mean_normal_generator = lambda N: torch.randn(N) / 1.5 + 3
normal_generator = lambda N: torch.randn(N) / 1.5
floor_from_normal_generator = lambda N: torch.floor(torch.randn(N))
degree_360_generator = lambda N: 360.0 * torch.rand(N)


def binormal_generator(N):
    sample = torch.randn(N) / 2 + 3
    displacement = 1.5 * torch.randint(0, 2, (N,))
    return sample + displacement


def binormal_generator_standard(N):
    sample = torch.randn(N) / 2 - 1
    displacement = 2 * torch.randint(0, 2, (N,))
    return sample + displacement


gauss_1d_lg = lambda N: torch.randn((N, 1))
gauss_2d_lg = lambda N: torch.randn((N, 2))
bigauss_1d_lg = lambda N: torch.randn((N, 1)) + 4 * torch.randint(0, 2, (N, 1))
uniform_1d_lg = lambda N: torch.rand((N, 1)) * 4 - 2
uniform_2d_lg = lambda N: torch.rand((N, 2)) * 4 - 2
